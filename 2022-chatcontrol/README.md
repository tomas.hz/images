Czech promotional material for the 2022 ChatControl petition.

- `poster1` - The first poster, to be printed on A3-sized paper or larger.
- `fliers` - Fliers to hand out and put in mailboxes. `-many` is the version to print, `-source` is the XCF source for a single one. Works best on A4 paper.
- `logo` - The logo.
- `signatures` - An A4-sized image to print and physically collect signatures on. Scrapped, was missing an area for a signature and used too much unnecessary space. An ODT in the chatcontrol repository's materials was used instead.
- `stand-top` - To be printed on 2 landscape oriented A3 pieces of paper next to each other, which are about the same size as the top of our petition stand's top bit.
- `stand-bottom` - To be printed on A2 paper. Large enough to go on the bottom bit of our petition stand. ~~Made because there were no pure Pirate Party bottom bits available at the time, only last year's coalition.~~ Made to include the domain, so people who are afraid of coming up to me can still look.
- `video` - A video.
- `slogan` - Slogans for the 14.06. and 02.07. demonstration.
- `information` - An A0-sized information board for the 14.06. demonstration.
- `organizer-card` - A card to recognize the organizer - myself - during the 14.06. demonstration.
- `second-protest-map` - A map of where the 02.07. demonstration will be held.
- `sticker` - A 75x45mm sticker.
- `protest-sign2` - A sign used at the 02.07. demonstration.
- `poster-third-protest` - A poster for the third demonstration, on 17.07.
- `poster-fourth-protest` - A poster for the fourth demonstration, on 31.07.
