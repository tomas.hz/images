Czech promotional material for the 2021 [ChatControl petition](https://openpetition.eu/!chatcontrolcz). The XCF source files have not survived, unfortunately.

1 - A full-size, A4 poster.
2 - Small, easily distributable fliers, `outside` to be folded around `inside` and stapled shut.
