Images made for the Czech Pirate Party.

`bitcoin-patrick-breyer`: https://zo.pirati.cz/aktuality/eu-parlament-utoci-na-kryptomeny.html
`poster-renesance`: https://renesancepiratu.cz. Only the second one will actually be used, the first one was scrapped.
`spolu.svg`: Logo of the SPOLU coalition. No SVG form was available online and I needed one, so I made it.
