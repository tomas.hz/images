Images related to Girls' Last Tour / Shoujo Shuumatsu Ryokou.

`chito_icon.png` - An icon of Chito, works well as the KDE applicatio
launcher icon. Original image in `chito_icon_original.png`. I chose to
remove the mouth, as it didn't look too good at such a low resolution.

`wallpaper1.mp4` - An animated wallpaper. `.tar.gz` is the Kdenlive
project archive, `.xcf` the GIMP file.
